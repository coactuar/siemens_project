function CTurnManager (){
   var _bClockWise;
   var _iNextPlayer;
   var _iThisTurn;  
   var _iStartingPlayer;
    
    this.init = function (){
        _bClockWise = true;
        
        _iStartingPlayer = 0;
        _iThisTurn = 0;
        _iNextPlayer=_iThisTurn+1;

        s_oTurnManager = this;
    };
    
    this.changeClockWise = function (){
        if (_bClockWise===true){
            _bClockWise=false;
        }else{
            _bClockWise=true;
        }
    };
    
    this.nextTurn = function(){
        if (_bClockWise===true){
            _iThisTurn++;
            if(_iThisTurn===NUM_PLAYERS){
                _iThisTurn = 0;
            }
            _iNextPlayer = _iThisTurn+1;
            if(_iNextPlayer === NUM_PLAYERS){
                _iNextPlayer = 0;
            }
        }else{
            _iThisTurn--;
            if(_iThisTurn<0){
                _iThisTurn = NUM_PLAYERS-1;
            }
            _iNextPlayer = _iThisTurn-1;
            if(_iNextPlayer<0){
                _iNextPlayer = NUM_PLAYERS-1;
            }
        } 
    };

    this.prevTurn = function(){
        if (_bClockWise===true){
            _iThisTurn--;
            _iNextPlayer = _iThisTurn+1;
            if (_iThisTurn<0){
                _iThisTurn = NUM_PLAYERS-1;
                _iNextPlayer = 0;
            }
        }else{
            _iThisTurn++;
            _iNextPlayer = _iThisTurn-1;
            if(_iThisTurn===NUM_PLAYERS){
                _iThisTurn = 0;
                _iNextPlayer = NUM_PLAYERS-1;
            }
        }
    };

    this.setTurn = function (iTurn){
        _iThisTurn = iTurn;
        
        if (_bClockWise===true){
            if (_iThisTurn===NUM_PLAYERS-1){
                _iNextPlayer=0;
            }else{
                _iNextPlayer=_iThisTurn+1;
            }
        }else{
            if (_iThisTurn===0){
                _iNextPlayer=NUM_PLAYERS-1;
            }else{
                _iNextPlayer=_iThisTurn-1;
            }
        }
    };
    
    this.setFirstPlayerToBegin = function(){
        this.setTurn(_iStartingPlayer);
        
        _iStartingPlayer++;
        if(_iStartingPlayer === NUM_PLAYERS){
            _iStartingPlayer = 0;
        }
    };
    
    this.resetClockWise = function(){
        _bClockWise = true;
    };
    
    this.resetFirstPlayer = function(){
        _iStartingPlayer = 0;
    };
    
    this.getTurn = function(){
        return _iThisTurn;
    };
    
    this.getNextPlayer= function(){
        return _iNextPlayer;
    };
    
    this.getClockWise = function (){
        return _bClockWise;
    };
    
    this.init();
};

s_oTurnManager = null;
