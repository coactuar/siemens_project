var CGameMulti = function(oData){
    CGameBase.call(this, oData);
    
    this._bActionInProgress;
    this._aMessageQueue;
    
    this._init();
    
};

CGameMulti.prototype = Object.create(CGameBase.prototype);

CGameMulti.prototype._init = function(){
    CGameBase.prototype._init();
    this._startGame();
};

CGameMulti.prototype._startGame = function(){
    this._bActionInProgress = true;
    this._aMessageQueue = new Array();
    
    this._oUnoController.addEventListener(ON_APPLY_EFFECT, this.applyEffectOnCard, this);
    this._oUnoController.addEventListener(ON_APPLY_PENALITY, this.applyPenality, this);
    this._oUnoController.addEventListener(ON_UNO_CLICK, this._onUnoClick, this);
    
    this._oSummaryPanel.addEventListener(ON_NEXT, this._onConfirmNextMatch, this);
    this._oSummaryPanel.addEventListener(ON_HOME, this.onExit, this);
    
    this._oMsgBox.addEventListener(ON_HOME, this.onExit, this);

    this._initHandPlayers();

    s_oNetworkManager.addEventListener(ON_STATUS_OFFLINE, this._onConnectionCrashed, this);
    
    s_oNetworkManager.sendMsg(MSG_REQUEST_PIECES, "");
};

CGameMulti.prototype.setNewGame = function(){
    for(var i=0; i<this._aPlayersHand.length; i++){
        this._aPlayersHand[i].setScore(0);
    }
    
    this._oTurnManager.resetFirstPlayer();
};

CGameMulti.prototype.restart = function(){
    this.reset();

    s_oNetworkManager.sendMsg(MSG_REQUEST_PIECES, "");
};

CGameMulti.prototype._initHandPlayers = function(){  
    var aHandPos = HAND_POS["num_player_"+NUM_PLAYERS];
    
    var aStartPos = new Array();
    for(var i=0; i<aHandPos.length; i++){
        var oPos = aHandPos[i];
        aStartPos.push({x:oPos.x, y:oPos.y, side:oPos.side});
    }
    
    var iClientIndexPlayer = s_oNetworkManager.getPlayerOrderID(); 
    var oShiftedElement;
    for(var i=0;i<iClientIndexPlayer;i++){
        oShiftedElement = aStartPos.pop();
        aStartPos.splice(0,0,oShiftedElement);
    }
    
    for(var i=0; i<NUM_PLAYERS; i++){
        
        var oPos = aStartPos[i];
        var iOffsetX =0;
        var iOffsetY =0;
        if (oPos.x===CANVAS_WIDTH/2){
            iOffsetX = CARD_WIDTH/2;
        }else{
            iOffsetY = CARD_HEIGHT/4;
        }
        this._aPlayersHand[i].setPosition(iOffsetX,iOffsetY,aStartPos[i].x, aStartPos[i].y, aStartPos[i].side);
        
        this._aPlayersHand[i].changeName(s_oNetworkManager.getNicknameByID(i));
    };
};

CGameMulti.prototype._onPiecesReceived = function(oData){
    this._oUnoController.setVisible(true);

    // DEBUG
    //STARTING_NUM_CARDS = 2;
    //var oData = [0,0,0,0,0,0,0,0,30,30,30,30,30,30,  20,20,1,1,53,53]; //EXAMPLE WITH +4
    //var oData = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,  53,20,23,20,24,20,25]; //START WITH ALL SPECIAL, TO TEST WITH NO FOCUS (2 players, 3 start cards)
    //var oData = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,  20,20,20,53,20,20,20,23,20,20,20,24,20,20,20,25]; //START WITH ALL SPECIAL, TO TEST WITH NO FOCUS (4 players, 4 start cards)
    //var oData = [0,0,0,0,0,0,    23,0,23,0];
    //var oData = [30,31,32,33,34,5,   23,0,20,0];
    
    this._oDeck.initializeFromData(oData);

    this.getFirstHand();
};

CGameMulti.prototype._checkFirstCardEffect = function(iEffect){
    switch(iEffect){
        case EFFECT_SELECT_COLOR:{
                this.onActionStop();
                
                this._oTurnManager.nextTurn();
                this._iCurPlayer = this._oTurnManager.getTurn();
                this._iNextPlayer = this._oTurnManager.getNextPlayer();
                
                if(this._iCurPlayer === s_oNetworkManager.getPlayerOrderID()){
                    this._applySelectColor();
                }
                
                this._oTurnManager.prevTurn();
                
                break;
        }
        case EFFECT_DRAW_FOUR:{
                ///CONSIDER TO SHUFFLE
                this.onActionStop();
                
                this._oTurnManager.nextTurn();
                this._iCurPlayer = this._oTurnManager.getTurn();
                this._iNextPlayer = this._oTurnManager.getNextPlayer();
                
                if(this._iCurPlayer === s_oNetworkManager.getPlayerOrderID()){
                    this._applyDrawFourEffect();
                    this._iCurPlayer = this._oTurnManager.getTurn();
                    this._iNextPlayer = this._iCurPlayer;
                }
                
                this._oTurnManager.prevTurn();

                break;
        }
        case EFFECT_STOP:{
                this._onActionBlockTurn();
                
                break;
        }
        case EFFECT_INVERT_TURN:{
                if(NUM_PLAYERS !== 2){
                    this._oTurnManager.nextTurn();
                }    
               
                this._onActionInvertTurn();
               
                break;
        }
        case EFFECT_DRAW_TWO_COLORED:{
                var oData = {playerindex: this._iNextPlayer};
                this._onActionDrawTwoColored(oData);

                break;
        }
        default:{
                //EFFECT_NORMAL_CARD
                this._oTurnManager.nextTurn();
                this._iCurPlayer = this._oTurnManager.getTurn();
                this._iNextPlayer = this._oTurnManager.getNextPlayer();
                
                if(this._iCurPlayer === s_oNetworkManager.getPlayerOrderID()){
                    this.onInputPlayer(this._iCurPlayer);
                    this._oDeck.enableInputDraw();
                }
                
                this._aPlayersHand[this._iCurPlayer].setOnTurn();
                
                this.onActionStop();

                break;
        }
    }
};

CGameMulti.prototype.onNextTurn = function(){
    this._bUNO = false;

    var iWinner = this.checkWinner();
    if (iWinner!==null){
        this.gameOver(iWinner);
    }else{
        /////OTHER HANDS
        this.setOffTurn();
        this._oDeck.enableInputDraw();
        
        this._oTurnManager.nextTurn();
        var iThisTurn = this._oTurnManager.getTurn();

        this._iCurPlayer = iThisTurn;
        this._iNextPlayer = this._oTurnManager.getNextPlayer();

        this._aPlayersHand[iThisTurn].setOnTurn();

        //console.log("iThisTurn:" + iThisTurn + " nextTurn:" + this._iNextPlayer + " s_oNetworkManager.getPlayerOrderID():" +s_oNetworkManager.getPlayerOrderID());

        if (iThisTurn===s_oNetworkManager.getPlayerOrderID()){
            this.onInputPlayer(iThisTurn);

            if (!this.playerCanPlay(iThisTurn)){
                this._oDeck.setHelp();
            }
        }
    }
    
    this.onActionStop();
};

CGameMulti.prototype.playCard = function(oCard,Event){
    var iThisTurn = this._oTurnManager.getTurn();
    var bCanPlay = false;
    var iPlayerID = s_oNetworkManager.getPlayerOrderID();
    if (iThisTurn===iPlayerID){
        bCanPlay = this.cardCanBePlayed(oCard, iThisTurn);
    }
    
    if(bCanPlay){
        this._oCardsContainer.addChildAt(this._oHandsContainer,this._oCardsContainer.numChildren);
        this._aPlayersHand[iThisTurn].setOnTop();
        this._oDeck.disableInputDraw();
        
        this.offInputPlayer(iPlayerID);

        ///SEND MESSAGE   
        var iCardIndex = oCard.getUniqueID();
        var oJSONData = {action: ACTION_USE_CARD, playerindex: iPlayerID, cardindex:iCardIndex};
        
        s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
    }
};

CGameMulti.prototype.playedCard = function(oCard){
    var iThisTurn = this._oTurnManager.getTurn();
    var iParent = this._aPlayersHand[iThisTurn].searchIndexCard(oCard);
    this._oUsedCards.pushCard(new CCard(0,0,this._oUsedCards.getContainer(),oCard.getFotogram(),oCard.getRank(),oCard.getSuit(),oCard.getUniqueID()));
    this._oUsedCards.disableInputUsedCards();
    this._oUsedCards.getLastCard().instantShow();
    this._aPlayersHand[iThisTurn].removeCardByIndex(iParent);
    oCard.unload();

    if(this._oUsedCards.getLastCard().getSuit()!==4){
        this._iCurrentColor = this._oUsedCards.getLastCard().getSuit();
        this._oInterface.refreshColor(this._iCurrentColor);
    }
    this._aPlayersHand[iThisTurn].organizeHand(iParent);
    
    this.onActionStop();
    
    //CARD ARRIVED, CONTROL HERE UNO
    if(iThisTurn === s_oNetworkManager.getPlayerOrderID()){
        this.checkUno(oCard.getEffect());
    }
};

CGameMulti.prototype._onUnoClick = function(){
    if (this._bUNO === true){
        this._bUNO = false;
        this._aPlayersHand[this._iCurPlayer].checkUno();
        
        var oJSONData = {action: ACTION_ON_UNO_CLICK, playerindex: this._iCurPlayer};
        s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
    }
};

//////////////////////APPLY EFFECTS ////////////////////////
CGameMulti.prototype.applyEffectOnCard = function(iEffect){
    if(this._iCurPlayer !== s_oNetworkManager.getPlayerOrderID()){
        return;
    }
    
    this._checkEffect(iEffect);
};


CGameMulti.prototype._applySelectColor = function(){
    //console.log("_applySelectColor");
    
    var oSelectColorPanel = new CSelectColorPanel(EFFECT_SELECT_COLOR);
    oSelectColorPanel.addEventListener(ON_COLOR_SELECTED, function(iColor){
        var oJSONData = {action: ACTION_SELECT_COLOR, playerindex: s_oNetworkManager.getPlayerOrderID(), colorindex:iColor};
        s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
    }, this);
};

CGameMulti.prototype._applyDrawFourEffect = function(){
    //console.log("_applyDrawFourEffect");
    
    var oSelectColorPanel = new CSelectColorPanel(EFFECT_DRAW_FOUR);
    oSelectColorPanel.addEventListener(ON_COLOR_SELECTED, function(iColor){
        var oJSONData = {action: ACTION_DRAW_FOUR, playerindex: this._iNextPlayer, colorindex:iColor};
        s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
    }, this);
};

CGameMulti.prototype._applyStopTurn = function(){
    ///SEND MESSAGE   
    var oJSONData = {action: ACTION_BLOCK_TURN/*, playerindex: s_oNetworkManager.getPlayerOrderID()*/};

    s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
};

CGameMulti.prototype._applyInvertTurn = function(){
    ///SEND MESSAGE   
    var oJSONData = {action: ACTION_INVERT_TURN/*, playerindex: s_oNetworkManager.getPlayerOrderID()*/};

    s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
};

CGameMulti.prototype._applyDrawTwoColored = function(){
    var oJSONData = {action: ACTION_DRAW_TWO_COLORED, playerindex: this._iNextPlayer};
    s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
};

CGameMulti.prototype._applyNormalCardEffect = function(){
    this._notifyChangeTurn();
};
/////////////////////////////////////////////////////

CGameMulti.prototype._notifyChangeTurn = function(){
    if(this._iCurPlayer !== s_oNetworkManager.getPlayerOrderID()){
        return;
    }
    
    //console.log("_notifyChangeTurn:"+this._iCurPlayer + " s_oNetworkManager.getPlayerOrderID():"+s_oNetworkManager.getPlayerOrderID())
    
    ///SEND MESSAGE   
    var oJSONData = {action: ACTION_NEXT_TURN, playerindex: s_oNetworkManager.getPlayerOrderID()};

    s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
};

CGameMulti.prototype.shuffleCards = function (iIndexPlayer,iNumberOfCards,iDelay,iDrawType){
    //console.log("SHUFFLECARDS--> iIndexPlayer:"+iIndexPlayer + " s_oNetworkManager.getPlayerOrderID():"+s_oNetworkManager.getPlayerOrderID())

    var aCardsObj = this._oUsedCards.removeAllCardUnderTheDeck();
    var aCardsFotogram = new Array();
    for(var i=0; i<aCardsObj.length; i++){
        aCardsFotogram.push(aCardsObj[i].getFotogram());
    }

    this._oAnimation.shuffleAnimation().then(()=>{
        this.onActionStop();
       
        if(this._iCurPlayer === s_oNetworkManager.getPlayerOrderID()){
            shuffle(aCardsFotogram);
            
            ///SEND MESSAGE   
            var oJSONData = {action: ACTION_ON_SHUFFLECARDS, playerindex: iIndexPlayer, numberofcards:iNumberOfCards, delay: iDelay, drawtype: iDrawType, cards: aCardsFotogram};
            s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
        }
    });
}; 

CGameMulti.prototype.drawCards = function (iIndexPlayer,iNumberOfCards,iDelay,iDrawType){
    //console.log("DRAWCARDS--> iIndexPlayer:"+iIndexPlayer + " s_oNetworkManager.getPlayerOrderID():"+s_oNetworkManager.getPlayerOrderID())

    ///SEND MESSAGE   
    var oJSONData = {action: ACTION_ON_DRAWCARDS, playerindex: iIndexPlayer, numberofcards:iNumberOfCards, delay: iDelay, drawtype: iDrawType};    
    s_oNetworkManager.sendMsg(MSG_MOVE, JSON.stringify(oJSONData));
    
}; 

CGameMulti.prototype._onAllCardsDrawCompleted = function(iIndexPlayer,iDrawType){
    this._checkEffectAfterDrawCompleted(iIndexPlayer,iDrawType);
    this.onActionStop();
};

CGameMulti.prototype._checkIfCanStillPlayTheTurn = function(iIndexPlayer){
    if (this.playerCanPlay(iIndexPlayer)){
        //console.log("CANPLAYACARD");
        this.onInputPlayer(iIndexPlayer);
    }else{
        //console.log("NOFIT");
        this._aPlayersHand[iIndexPlayer].centerContainer();
        //this._oTurnManager.nextTurn();

        this._notifyChangeTurn();
    }
};


////////////////ACTIONS //////////////////////////
CGameMulti.prototype.onActionReceived = function(oData){
    ////CALL THIS WHENEVER AN ACTION IS RECEIVED FROM SERVER
    this._aMessageQueue.push(oData);
    this._evaluateMessageQueue();
};

CGameMulti.prototype.onActionStop = function(){
    ////CALL THIS WHENEVER AN ACTION IS STOPPED
    this._bActionInProgress = false;
    this._evaluateMessageQueue();
};

CGameMulti.prototype._evaluateMessageQueue = function(){
    if(this._aMessageQueue.length===0 || this._bActionInProgress){
        return;
    }

    this._bActionInProgress = true;
    var oMessage = this._aMessageQueue.shift();

    ///MAP HERE ALL THE ACTION OF PLAYERS THAT SHOULD BE QUEUED
    switch(oMessage.action){
        case ACTION_NEXT_TURN:{
                this.onNextTurn();
                break;
        }
        case ACTION_USE_CARD:{
                this._onActionPlayCard(oMessage);
                break;
        }
        case ACTION_ON_SHUFFLECARDS:{
                this._onActionShuffleCard(oMessage);
                break;
        }
        case ACTION_ON_DRAWCARDS:{
                this._onActionDrawCards(oMessage);
                break;
        }
        case ACTION_ON_UNO_CLICK:{
                this._onActionUnoClick(oMessage);
                break;
        }
        case ACTION_SELECT_COLOR:{
                this._onActionSelectColor(oMessage);
                break;
        }
        case ACTION_DRAW_FOUR:{
                this._onActionDrawFour(oMessage);
                break;
        }
        case ACTION_BLOCK_TURN:{
                this._onActionBlockTurn(oMessage);
                break;
        }
        case ACTION_INVERT_TURN:{
                this._onActionInvertTurn(oMessage);
                break;
        }
        case ACTION_DRAW_TWO_COLORED:{
                this._onActionDrawTwoColored(oMessage);
                break;
        }
    }
};

CGameMulti.prototype._onActionPlayCard = function(oData){
    var iThisTurn = oData.playerindex;
    var oHandGlobalPos =this._aPlayersHand[iThisTurn].getContainerPos();
    var oUsedCardsGlobalPos = this._oUsedCards.getGlobalPosition();
    
    var oCard = this._aPlayersHand[iThisTurn].getCardByUniqueID( oData.cardindex );   
    
    playSound("card",1,false);
    
    oCard.moveCard((oUsedCardsGlobalPos.x-oHandGlobalPos.x),oUsedCardsGlobalPos.y-oHandGlobalPos.y,300);
    oCard.showCard();
    
    this._oUsedCards.setChildDepth(2);
};

CGameMulti.prototype._onActionShuffleCard = function(oData){
    var iIndexPlayer = oData.playerindex;
    var iNumberOfCards = oData.numberofcards;
    var iDelay = oData.delay;
    var iDrawType = oData.drawtype;
    var aCards = oData.cards;
    
    this._oDeck.clearCards();
    this._oDeck.initializeFromData(aCards);

    this.checkForMoreDraws(iIndexPlayer,iNumberOfCards,iDelay,iDrawType);
};

CGameMulti.prototype._onActionDrawCards = function(oData){
    var iDrawType = oData.drawtype;
    var iNumberOfCards = oData.numberofcards;
    var iDelay = oData.delay;
    var iIndexPlayer = oData.playerindex;
    
    var bPenality = iDrawType === DRAW_TYPE_PENALITY ? true : false; 
    this._checkUnoNotify(bPenality, s_oNetworkManager.getPlayerOrderID(), iIndexPlayer);
    
    this._checkNumberCardsToDraw(iIndexPlayer,iNumberOfCards,iDelay,iDrawType);
};

CGameMulti.prototype._onActionUnoClick = function(oData){
    var iIndexPlayer = oData.playerindex;
    
    this._aPlayersHand[iIndexPlayer].checkUno();
    
    this.onActionStop();
};

CGameMulti.prototype._onActionSelectColor = function(oData){
    this._iCurrentColor = oData.colorindex;
    
    this._oAnimation.changeColor(this._iCurrentColor).then(()=> {
        this._oInterface.refreshColor(this._iCurrentColor);

        this._notifyChangeTurn();

        this.onActionStop();
    });
};

CGameMulti.prototype._onActionDrawFour = function(oData){
    this._iCurrentColor = oData.colorindex;
    
    this._oAnimation.changeColor(this._iCurrentColor).then(()=> {
        this._oInterface.refreshColor(this._iCurrentColor);

        var iDrawType = DRAW_TYPE_DRAW4;
        var iNumberOfCards = NUM_WILD_CARDS;
        var iDelay = 0;
        var iIndexPlayer = oData.playerindex;

        oData = {playerindex: iIndexPlayer, numberofcards:iNumberOfCards, delay: iDelay, drawtype: iDrawType}

        this._onActionDrawCards(oData);
    });
};

CGameMulti.prototype._onActionBlockTurn = function(oData){
    this._oAnimation.stopTurn().then(()=> {
        this._oTurnManager.nextTurn();
        this._notifyChangeTurn();
        this.onActionStop();
    });
};

CGameMulti.prototype._onActionInvertTurn = function(oData){
    this._oTurnManager.changeClockWise();
    if(NUM_PLAYERS === 2){
        ///IN 2 PLAYERS THE CARD BEHAVIOUR IS DIFFERENT
        this._oTurnManager.nextTurn();
    }

    this._oAnimation.changeClockWise(s_oGame._oTurnManager.getClockWise()).then(()=> {
        this._notifyChangeTurn();
        this.onActionStop();
    });
};

CGameMulti.prototype._onActionDrawTwoColored = function(oData){
    var iDrawType = DRAW_TYPE_DRAW2_COLORED;
    var iNumberOfCards = 2;
    var iDelay = 0;
    var iIndexPlayer = oData.playerindex;

    oData = {playerindex: iIndexPlayer, numberofcards:iNumberOfCards, delay: iDelay, drawtype: iDrawType}

    this._onActionDrawCards(oData);
};
////////////////////////////////////////////////////


CGameMulti.prototype.onExit = function(){
    
    s_oGame.unload();
    s_oMain.gotoMenu();

    $(s_oMain).trigger("end_session");
    $(s_oMain).trigger("show_interlevel_ad");
    
    s_oNetworkManager.disconnect();
};

////////NEXT MATCH
CGameMulti.prototype._onConfirmNextMatch = function(){
    $(s_oMain).trigger("show_interlevel_ad");
    
    this._oSummaryPanel.waitingMode();

    s_oNetworkManager.sendMsg(MSG_ACCEPT_NEXTMATCH, "");
};

CGameMulti.prototype.onOpponentAcceptNextMatch = function(){
    if(this._bEndGame){
        this._bEndGame = false;
        this.setNewGame();
    };
    
    this.restart();
};

CGameMulti.prototype.opponentLeftTheGame = function(iRefusedPlayerID){
    var szPlayerName = s_oNetworkManager.getNicknameByID(iRefusedPlayerID);
    
    if(this._oSummaryPanel.isShown()){
        this._oSummaryPanel.playerQuitMode( sprintf(TEXT_QUIT_FROM_GAME, szPlayerName) );
    }else {
        if(!this._oMsgBox.isShown()){
            this._oSummaryPanel.hide();

            this._oMsgBox.show(sprintf(TEXT_QUIT_FROM_GAME, szPlayerName));
        }
    }
};

CGameMulti.prototype._onConnectionCrashed = function(){
    this._oSummaryPanel.hide();
    this._oMsgBox.show(TEXT_SOMETHING_WENT_WRONG);
};