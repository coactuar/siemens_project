var ON_CONNECTION_ERROR = 0;
var ON_DISCONNECTION = 1;
var ON_DISCONNECTION_FROM_MATCH = 2;
var ON_LOGIN_SUCCESS = 3;
var ON_MATCHMAKING_CONNECTION_SUCCESS = 4;
var ON_GAMEROOM_CONNECTION_SUCCESS = 5;
var ON_USEROWNERROOM_CREATE_SUCCESS = 6;
var ON_USEROWNERROOM_JOIN_SUCCESS = 7;
var ON_ROOM_INFO_RETURNED = 8;
var ON_BACK_FROM_A_ROOM = 9;

var ERROR_CODE_UNKNOWNROOM = "UnknownRoom";

var ON_STATUS_ONLINE = "online";
var ON_STATUS_OFFLINE = "offline";

/////////////// ROOM TYPE
var ROOM_TYPE_MATCHMAKING = 'MatchmakingRoom';
var ROOM_TYPE_USEROWNER = 'UserOwnerRoom';
var ROOM_TYPE_GAME = "GameRoom";

var WAITING_PLAYERS_TIMEOUT = 10000;

function CNetworkManager(){
    
    var _bStatusOnline;
    
    var _aCbCompleted;
    var _aCbOwner;
    var _aRoomsOwnerList;
    var _aRoomsGameList;
    
    var _oCurConnection;
    var _oCurClient;
    var _oMessageForwarder;
    
    var _iPlayerOrderID;
    var _iMaxPlayers;
    var _iNumGamePlayers;
    var _iTypeRoomsListedCounter;
    var _iIdWaitingPlayersTimeout;
    var _iNumBot;
    
    var _szNickname;
    var _aNicknameList;
    var _aAvatarList;
    
    var _szCurRoomID;
    var _szCurRoomPass;
    var _szBotName;
    
    var _oThis;
    
    this._init = function(){
        _bStatusOnline = navigator.onLine;
        
        _aCbCompleted = new Array();
        _aCbOwner = new Array();
        _aRoomsOwnerList = new Array();
        _aRoomsGameList = new Array();
        
        _iTypeRoomsListedCounter = 0;
        _iNumBot = 0;
        
        window.addEventListener('online',  this._onConnectionChangeStatusOnline);
        window.addEventListener('offline',  this._onConnectionChangeStatusOffline);
        
        _oMessageForwarder = new CNetworkMessageForwarder();
        
        _oThis = this;
    };

    this.unload = function(){
        s_oNetworkManager = null;
    };
    
    this.disconnectFromSystem = function(){
        g_oCTLMultiplayer.closeAllDialog();
        _oThis.disconnectFromCurRoom();
    };
    
    this.connectToSystem = function(){
        if(_bStatusOnline){
            g_oCTLMultiplayer.showChooseNickName();
        }else {
            _oThis.showErrorMessage(TEXT_NO_CONNECTION);
        }
    };

    this.login = function(szNickname){
        _szNickname = szNickname;
        
        var szIDNickname = _oThis._setValidNick(szNickname);
        
        PlayerIO.useSecureApiRequests = !MULTIPLAYER_TEST_LOCAL;	
        
        PlayerIO.authenticate(GAME_PLAYERIO_ID, "public", { userId: szIDNickname }, {}, function (client) {
            _oCurClient = client;
            _oCurClient.multiplayer.useSecureConnections = !MULTIPLAYER_TEST_LOCAL;
            //console.log("Authenticated to PlayerIO as: " + client.connectUserId);

            if(MULTIPLAYER_TEST_LOCAL){
                
                _oCurClient.multiplayer.developmentServer = 'localhost:8184';
                
            }
            
            _oThis.gotoLobby();
            
            if(_aCbCompleted[ON_LOGIN_SUCCESS]){
                _aCbCompleted[ON_LOGIN_SUCCESS].call(_aCbOwner[ON_LOGIN_SUCCESS]);
            }
            
        }, _oThis.callbackError);
    };

    this._setValidNick = function(szNickname){
        var szValidNickname;
        var szCodeNumber = _oThis._getRandomCodeNumber();
        
        ///EMPTY CASE
        if(szNickname === ""){
            szValidNickname = "guest-"+szCodeNumber;
            _szNickname = szValidNickname;
        } else {
            szValidNickname = szNickname +"-"+ szCodeNumber;
        }
        
        return szValidNickname; 
    };

    this._getRandomCodeNumber = function(){
        return Math.floor( Math.random()*1000);
    };


    this.generateRandomName = function(){
        var aListName = [
            "xmariox", "alex", "max", "mahuro", "biajus", "rob", "idah", "fabrix", "seth", "ikillyou", "commander", "admiral", "general", "seasalt", "emperorofthesea",
            "Aspect","Kraken","Dragon","Shiver","Dracula","Doom","Scar","Roadkill","Cobra","Psycho","Ranger","Ripley","Clink","Bruise","Bowser","Creep","Cannon","Daemon",
            "Steel","Tempest","Hurricane","Titanium","Tito", "Lightning", "IronHeart", "Sabotage" ,"Rex", "Hydra", "Terminator", "Agrippa", "Gash",
            "Blade","Katana","Gladius","Angon","Claymore","Pike","Hammer","Club","Heart","Gauntlet","Montante","Longbow","bow","Dagger"
        ];

        var iRandomIndex = Math.floor( Math.random()*aListName.length );
        var szRandomName = aListName[iRandomIndex];

        /////SET RANDOM NUMBER
        if(Math.random()> 0.5){
            var szRandomNumber = Math.floor( Math.random()*100 );
            /////ADD SPECIAL CHAR
            if(Math.random() > 0.5){
                var aSpecial = ["-", "_"];
                var iRandomIndex = Math.floor( Math.random()*aSpecial.length );
                var szSpecialChar = aSpecial[iRandomIndex];

                szRandomName += szSpecialChar;
            }
            szRandomName += szRandomNumber;
        }

        _szBotName = szRandomName;
        
        return szRandomName;
    };  

    this.getBotName = function(){
        return _szBotName;
    };

    this._onConnectionChangeStatusOnline = function(event){
        _bStatusOnline = true;
        
        if(_aCbCompleted[ON_STATUS_ONLINE]){
            _aCbCompleted[ON_STATUS_ONLINE].call(_aCbOwner[ON_STATUS_ONLINE]);
        }
        
        if ( jQuery(".ctl-multiplayer-dlg-content").length > 0 ){
            _oThis._onReconnection();
        }
    };

    this._onConnectionChangeStatusOffline = function(){
        _bStatusOnline = false;
        
        if(_aCbCompleted[ON_STATUS_OFFLINE]){
            _aCbCompleted[ON_STATUS_OFFLINE].call(_aCbOwner[ON_STATUS_OFFLINE]);
        }
        
        if ( jQuery(".ctl-multiplayer-dlg-content").length > 0 ){
            _oThis.showErrorMessage(TEXT_NO_CONNECTION);
            
            _oThis.disconnect();
        }
    };

    this._onReconnection = function(){
        if(_szNickname === null || _szNickname === undefined){
            g_oCTLMultiplayer.closeAllDialog();
            _oThis.connectToSystem();
        }else {
            g_oCTLMultiplayer.closeAllDialog();
            _oThis.gotoLobby();
        }
    };

    this.showErrorMessage = function(szMsg){
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showGeneralDialog( szMsg, "g_oCTLMultiplayer.closeAllDialog");
    };

    this.addEventListener = function(iEvent,cbCompleted, cbOwner){
        _aCbCompleted[iEvent]=cbCompleted;
        _aCbOwner[iEvent] = cbOwner; 
    };
    
    ///////////////////////////////// EVENT FUNCTIONS
    //Log all errors to console
    this.callbackError = function(error) {
        console.log("Error: " + error.code + " - " + error.message);
        if(_aCbCompleted[ON_CONNECTION_ERROR]){
            _aCbCompleted[ON_CONNECTION_ERROR].call(_aCbOwner[ON_CONNECTION_ERROR], error);
        }

        switch(szError.code){
            case ERROR_CODE_UNKNOWNROOM: {
                    g_oCTLMultiplayer.closeAllDialog();
                    g_oCTLMultiplayer.showGeneralDialog(TEXT_ROOM_DOESNT_EXIST, "s_oNetworkManager.gotoLobby");
                break;
            }
            default :{
                    _oThis.showErrorMessage(error);   
            }
        }
    };
    
    //Log disconnection
    this.callbackDisconnect = function(error) {
        console.log("Disconnected From Menu");
        if(_aCbCompleted[ON_DISCONNECTION]){
            _aCbCompleted[ON_DISCONNECTION].call(_aCbOwner[ON_DISCONNECTION], error);
        }
    };
    
    this.callbackDisconnectFromMatch = function(error){
        console.log("Disconnected From Match");
        if(_aCbCompleted[ON_DISCONNECTION_FROM_MATCH]){
            _aCbCompleted[ON_DISCONNECTION_FROM_MATCH].call(_aCbOwner[ON_DISCONNECTION_FROM_MATCH], error);
        }
    };
    
    //////////////////// COMMUNICATION TO SERVER /////////
    this.sendMsg = function(szMessage, oParam){
        if(_oCurConnection){
            _oCurConnection.send(szMessage, oParam);
        }
    };
    //////////////////////////////////////////////////////

    this.disconnect = function(){
        if(_oCurConnection){
            _oCurConnection.disconnect();
            _oCurConnection = null;
        }
        
    };
    
    this.isUserA = function(){
        return parseInt(_iPlayerOrderID) === 0 ? true:false;
    };
    
    this.getPlayerOrderID = function(){
        return _iPlayerOrderID;
    };
    
    this.getPlayerNickname = function(){
        return _szNickname;
    };
    
    this.getNicknameByID = function(iID){
        return _aNicknameList[iID];
    };
    
    this.getNicknameList = function(){
        return _aNicknameList;
    };
    
    this.getAvatarByID = function(iID){
        return _aAvatarList[iID];
    };
    
    this.getAvatarList = function(){
        return _aAvatarList;
    };
    
    ////////////////////////////////////////////////////////////
    ///////////////////// ROOMS SYSTEM FUNCTIONS////////////////

    this.createRoom = function(szRoomID, szPass, iMaxPlayers){
        //Use local development server
        if(MULTIPLAYER_TEST_LOCAL){
            _oCurClient.multiplayer.developmentServer = 'localhost:8184';
        }
        console.log("maxusers:"+iMaxPlayers)
        //Join the room
        _oCurClient.multiplayer.createJoinRoom(szRoomID, ROOM_TYPE_USEROWNER, true, {id: szRoomID, pass: szPass, curusers:1, maxusers: iMaxPlayers}, {nickname:_szNickname, avatar:""}, function (connection) {

            //console.log("create owner room for user: " + _oCurClient.connectUserId);
            _oCurConnection = connection;
            connection.addMessageCallback("*", _oMessageForwarder.messageHandler);
            connection.addDisconnectCallback(_oThis.callbackDisconnect);

            _oThis._onRoomCreated();
            
            if(_aCbCompleted[ON_USEROWNERROOM_CREATE_SUCCESS]){
                _aCbCompleted[ON_USEROWNERROOM_CREATE_SUCCESS].call(_aCbOwner[ON_USEROWNERROOM_CREATE_SUCCESS]);
            }


        }, _oThis.callbackError);
    };
    
    this.joinRoom = function(szRoomID){
        //console.log("join room as: " + _oCurClient.connectUserId);

        _szCurRoomID = szRoomID;

        //Use local development server
        if(MULTIPLAYER_TEST_LOCAL){
            _oCurClient.multiplayer.developmentServer = 'localhost:8184';
        }

        //Join the room
        _oCurClient.multiplayer.joinRoom(szRoomID, {nickname:_szNickname, avatar:""}, function (connection) {
            _oCurConnection = connection;
            connection.addMessageCallback("*", _oMessageForwarder.messageHandler);
            connection.addDisconnectCallback(_oThis.callbackDisconnect);

            _oThis._onRoomJoined();

            if(_aCbCompleted[ON_USEROWNERROOM_JOIN_SUCCESS]){
                _aCbCompleted[ON_USEROWNERROOM_JOIN_SUCCESS].call(_aCbOwner[ON_USEROWNERROOM_JOIN_SUCCESS]);
            }
        }, _oThis.callbackError);
    };
    
    this.gotoGameRoom = function(oMessage){
        //Use local development server
        if(MULTIPLAYER_TEST_LOCAL){
            _oCurClient.multiplayer.developmentServer = 'localhost:8184';
        }

        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showLoading(TEXT_MATCH_FOUND.toUpperCase());

        ///////COLLECT DATA
        var szRoomID = oMessage.getString(0);
        //console.log("szRoomID:"+szRoomID);
        _iPlayerOrderID = oMessage.getInt(1);
        //console.log("_iPlayerOrderID:"+_iPlayerOrderID);
        /*
        var szNicknameList = oMessage.getString(2);
        _aNicknameList = JSON.parse(szNicknameList);
        //console.log(_aNicknameList);
        
        var szAvatarList = oMessage.getString(3);
        _aAvatarList = JSON.parse(szAvatarList);
        //console.log(_aAvatarList);
        
        _iNumGamePlayers = _aNicknameList.length;

        _iNumBot = oMessage.getInt(4);
        */
        
        var oPlayersInfo = oMessage.getString(2);
        var oPlayersObj = JSON.parse(oPlayersInfo);
        var aPlayersInfo = Object.values(oPlayersObj);

        _aNicknameList = new Array();
        _aAvatarList = new Array();
        for(var i=0; i<aPlayersInfo.length; i++){
            _aNicknameList[i] = aPlayersInfo[i].nickname;
            _aAvatarList[i] = aPlayersInfo[i].avatar;
        }

        _iNumGamePlayers = aPlayersInfo.length;

       // console.log("gotoGameRoom")

        //Join the room
        _oCurClient.multiplayer.createJoinRoom(szRoomID, ROOM_TYPE_GAME, true, {/*numgameplayers: _iNumGamePlayers,*/ pass:"", /*curusers:_iNumGamePlayers,*/ maxusers:_iNumGamePlayers,/*, jokeravailable: JOKER_AVAILABLE*/playersinfo: oPlayersInfo}, {ingameid: _iPlayerOrderID}, function (connection) {     
                _oThis.disconnectFromCurRoom();
                _oCurConnection = connection;
                connection.addMessageCallback("*", _oMessageForwarder.messageHandler);
                connection.addDisconnectCallback(_oThis.callbackDisconnectFromMatch);

                //console.log("game found! move players to a game room");

                //g_oCTLMultiplayer.closeAllDialog();

                if(_aCbCompleted[ON_GAMEROOM_CONNECTION_SUCCESS]){
                    _aCbCompleted[ON_GAMEROOM_CONNECTION_SUCCESS].call(_aCbOwner[ON_GAMEROOM_CONNECTION_SUCCESS], _iNumGamePlayers);
                }
                
                _oThis._onGameRoomEntered();
                
        }, _oThis.callbackError);
    };
    
    
    this.gotoGameRoomWithBot = function(){
        ///ITS USE IS JUST TO FILL IN LIST ROOMS
        
        //Use local development server
        if(MULTIPLAYER_TEST_LOCAL){
            _oCurClient.multiplayer.developmentServer = 'localhost:8184';
        }
        
        var szRoomID = randomFloatBetween(1000000, 2000000,0) + "";
        
        _iPlayerOrderID = 0;
        
        _aNicknameList = new Array();
        _aNicknameList[0] = _szNickname;
        _aNicknameList[1] = _szBotName;
        
        _aAvatarList = new Array();
        _aAvatarList[0] = "";
        _aAvatarList[1] = "";
        
        //Join the room
        _oCurClient.multiplayer.createJoinRoom(szRoomID, ROOM_TYPE_GAME, true, {/*numgameplayers: 2,*/ pass:"", /*curusers:2,*/ maxusers:2, bot:true}, {ingameid: _iPlayerOrderID}, function (connection) {
                _oThis.disconnectFromCurRoom();
                _oCurConnection = connection;
                connection.addMessageCallback("*", _oMessageForwarder.messageHandler);
                connection.addDisconnectCallback(_oThis.callbackDisconnectFromMatch);

                //console.log("game found! move players to a game room");

                g_oCTLMultiplayer.closeAllDialog();

        }, _oThis.callbackError);
    };  
    
    this.gotoMatchMakingRoom = function(){
        //Use local development server
        if(MULTIPLAYER_TEST_LOCAL){
            _oCurClient.multiplayer.developmentServer = 'localhost:8184';
        }

        //Join the room
        _oCurClient.multiplayer.createJoinRoom('matchmakingroom1', ROOM_TYPE_MATCHMAKING, true, null, {nickname:_szNickname, avatar:""}, function (connection) {
                _oCurConnection = connection;
                connection.addMessageCallback("*", _oMessageForwarder.messageHandler);
                connection.addDisconnectCallback(_oThis.callbackDisconnect);

                //console.log("Connected to matchmaking room");

                if(_aCbCompleted[ON_MATCHMAKING_CONNECTION_SUCCESS]){
                    _aCbCompleted[ON_MATCHMAKING_CONNECTION_SUCCESS].call(_aCbOwner[ON_MATCHMAKING_CONNECTION_SUCCESS]);
                }
                
                g_oCTLMultiplayer.closeAllDialog();
                g_oCTLMultiplayer.showLoading(TEXT_FIND_OPPONENT, "s_oNetworkManager._onDisconnectFromARoom");
        

        }, _oThis.callbackError);
    };

    this._onGameRoomEntered = function(){
        _iIdWaitingPlayersTimeout = setTimeout(function(){
            g_oCTLMultiplayer.closeAllDialog();
            g_oCTLMultiplayer.showGeneralDialog(TEXT_OPPONENT_LEFT, "s_oNetworkManager.gotoLobby");
            _oThis.disconnect();
        }, WAITING_PLAYERS_TIMEOUT);
    };

    this.onGameStarted = function(){
        g_oCTLMultiplayer.closeAllDialog();
        clearTimeout(_iIdWaitingPlayersTimeout);
    };

    this.tryCreateUniqueRoom = function(szRoomID, szPass, iMaxPlayers ){
        _szCurRoomID = szRoomID;
        _szCurRoomPass = szPass;
        _iMaxPlayers = iMaxPlayers;
        
        _oCurClient.multiplayer.listRooms(ROOM_TYPE_USEROWNER, {id: szRoomID}, 0,0,_oThis._onUniqueListRoomSearch, _oThis.callbackError);
    };
    
    this._onUniqueListRoomSearch = function(aRooms){
        if(aRooms.length > 0){
            ///ANOTHER ROOM WITH SAME NAME EXIST!
            _szCurRoomID += "-" + _oThis._getRandomCodeNumber();
        }
        
        _oThis.createRoom(_szCurRoomID, _szCurRoomPass, _iMaxPlayers);
    };

    this._onPlayNow = function(){
        _oThis.sendMsg(MSG_PLAYNOW, "");
    };
    
    this._addBotInRoom = function(){
        if(!COMBINED_PLAYERS_MODE){
            return;
        }
        _oThis.sendMsg(MSG_ADDBOT, _oThis.generateRandomName()+"-bot");
        ////prevent accidentally double click
        jQuery('.ctl-multiplayer-room-add-bot').addClass('ctl-multiplayer-display-none');
        jQuery('.ctl-multiplayer-room-remove-bot').addClass('ctl-multiplayer-display-none');
    };
    
    this._removeBotInRoom = function(){
        if(!COMBINED_PLAYERS_MODE){
            return;
        }
        _oThis.sendMsg(MSG_REMOVEBOT, "");
        ////prevent accidentally double click
        jQuery('.ctl-multiplayer-room-add-bot').addClass('ctl-multiplayer-display-none');
        jQuery('.ctl-multiplayer-room-remove-bot').addClass('ctl-multiplayer-display-none');
    };
    
    this.onBotAdded = function(iNumBot){
        if(!COMBINED_PLAYERS_MODE){
            return;
        }
        //console.log("BOTADDED:"+iNumBot)

        jQuery('.ctl-multiplayer-room-add-bot').removeClass('ctl-multiplayer-display-none');
        jQuery('.ctl-multiplayer-room-remove-bot').removeClass('ctl-multiplayer-display-none');
    };
    
    this.onBotRemoved = function(iNumBot){
        if(!COMBINED_PLAYERS_MODE){
            return;
        }
        //console.log("BOTREMOVED:"+iNumBot);
        
        if(iNumBot>0){
            jQuery('.ctl-multiplayer-room-add-bot').removeClass('ctl-multiplayer-display-none');
            jQuery('.ctl-multiplayer-room-remove-bot').removeClass('ctl-multiplayer-display-none');
        }else {
            jQuery('.ctl-multiplayer-room-add-bot').removeClass('ctl-multiplayer-display-none');
            jQuery('.ctl-multiplayer-room-remove-bot').addClass('ctl-multiplayer-display-none');
        }
    };
    
    this.disconnectFromCurRoom = function(){
        if(_oCurConnection){
            ////SILENT DISCONNECTION. PREVENT USELESS DISCONNECTION MESSAGE
            console.log("TEST")
            _oCurConnection.removeDisconnectCallback(_oThis.callbackDisconnect);
            _oCurConnection.disconnect();
        }
    };
    
    this._onDisconnectFromARoom = function(){
        if(_aCbCompleted[ON_BACK_FROM_A_ROOM]){
            _aCbCompleted[ON_BACK_FROM_A_ROOM].call(_aCbOwner[ON_BACK_FROM_A_ROOM]);
        }
        
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showLoading(TEXT_CONNECT_TO_LOBBY);
        _oThis.disconnectFromCurRoom();

        ///Seems there is some delay to listrooms when you delete a room. Even with a callback to disconnect function
        setTimeout(function(){
            _oThis.gotoLobby();
        }, 500);
    };

    this._onRoomCreated = function(){
        ///////// USER CREATED AND JOINED A ROOM
        
        var oPlayNowButton = { func : "s_oNetworkManager._onPlayNow", label : TEXT_PLAY_NOW, classes : "ctl-multiplayer-room-play-now ctl-multiplayer-display-none" };
        var oBackButton = { func : "s_oNetworkManager._onDisconnectFromARoom", label : TEXT_SYS_BACK };
        var oAddBotButton = { func : "s_oNetworkManager._addBotInRoom", label : TEXT_SYS_ADD_BOT, classes : "ctl-multiplayer-room-add-bot ctl-multiplayer-display-block"}
        var oRemoveBotButton = { func : "s_oNetworkManager._removeBotInRoom", label : TEXT_SYS_REMOVE_BOT, classes : "ctl-multiplayer-room-remove-bot ctl-multiplayer-display-none"}
        
        var aButtonList = [oPlayNowButton, oBackButton];
        if(COMBINED_PLAYERS_MODE){
            aButtonList.push(oAddBotButton, oRemoveBotButton);
        }
        
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showWaitingPlayersInRoom(TEXT_WAITING_FOR_PLAYERS_IN_ROOM +_szCurRoomID,
                [_szNickname], 
                aButtonList
                        );
    };
    
    this._onRoomJoined = function(){
        ///////// USER JOINED A ROOM
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showWaitingPlayersInRoom(TEXT_WAITING_FOR_PLAYERS_IN_ROOM +_szCurRoomID,
                [], 
                [
                    { func : "s_oNetworkManager._onDisconnectFromARoom", label : TEXT_SYS_BACK }
                ]);
    };

    this.gotoLobby = function(){
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showLoading(TEXT_CONNECT_TO_LOBBY);
        
        _oThis.refreshRooms();
    };
    
    this.refreshRooms = function(){
        _oCurClient.multiplayer.listRooms(ROOM_TYPE_USEROWNER, null, 0,0,_oThis._onFillWithOwnerRoom, _oThis.callbackError);
        _oCurClient.multiplayer.listRooms(ROOM_TYPE_GAME, null, 0,0,_oThis._onFillWithGameRoom, _oThis.callbackError);
    };
    
    this._onFillWithOwnerRoom = function(aRooms){
        _iTypeRoomsListedCounter++;
        
        _oThis._extractOwnerRoomInfo(aRooms);
 
        if(_iTypeRoomsListedCounter >= 2){
            _oThis._onFinalUpdateRoomList();
        }
        
        //console.log(aRooms)
    };
    
    this._onFillWithGameRoom = function(aRooms){
        _iTypeRoomsListedCounter++;
        
        _oThis._extractGameRoomInfo(aRooms);
        
        if(_iTypeRoomsListedCounter >= 2){
            _oThis._onFinalUpdateRoomList();
        }
        
        //console.log(aRooms)
    };
    
    this._onFinalUpdateRoomList = function(){
        var aCombRooms = _oThis._getCombinedRoomsLists();
        
        if ( jQuery(".ctl-multiplayer-room-list").length > 0 ){
            g_oCTLMultiplayer.refreshRoomList(aCombRooms);
        }else{
            this._showLobby(aCombRooms);                
        }       
        
        var szMaxHeight = "400px";
        if(s_bMobile){
            szMaxHeight = "200px";
        }
        $(".ctl-multiplayer-room-list").css("max-height", szMaxHeight)
    };

    this._showLobby = function(aRooms){
        _iTypeRoomsListedCounter = 0;
        
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showRoomList(aRooms);
        //_oThis._autoRefreshRooms();
    };
    
    this._extractOwnerRoomInfo = function(aRooms){
        _aRoomsOwnerList = new Array();
        for(var i=0; i<aRooms.length; i++){
            var bPrivate = aRooms[i].roomData.pass.length === 0 ? false : true;
            
            _aRoomsOwnerList[i] = {
                                       name: aRooms[i].id, 
                                       private:bPrivate,
                                       accessible : true,
                                       curusers: aRooms[i].roomData.curusers,
                                       maxusers: aRooms[i].roomData.maxusers
                                    };
        }
        
        return _aRoomsOwnerList;
    };

    this._extractGameRoomInfo = function(aRooms){
        _aRoomsGameList = new Array();
        for(var i=0; i<aRooms.length; i++){
            var bPrivate = aRooms[i].roomData.pass.length === 0 ? false : true;
            _aRoomsGameList[i] = { 
                                   name: "game-"+aRooms[i].id, 
                                   private: bPrivate,
                                   accessible : false,
                                   curusers: aRooms[i].roomData.curusers,
                                   maxusers: aRooms[i].roomData.maxusers
                                  };
        }
        
        return _aRoomsGameList;
    };
    
    this._getCombinedRoomsLists = function(){
        var aRooms = new Array();
        for(var i=0; i<_aRoomsOwnerList.length; i++){
            aRooms.push(_aRoomsOwnerList[i]);
        }
        for(var i=0; i<_aRoomsGameList.length; i++){
            aRooms.push(_aRoomsGameList[i]);
        }
        
        return aRooms;
    };
    
    
    
    this.joinQuickMatch = function(){
        g_oCTLMultiplayer.showLoading(TEXT_NETWORK_CONNECTING);
        
        _oThis.gotoMatchMakingRoom();
    };

    this.tryJoinRoomWithPass = function(szRoomID, szPass){
        _szCurRoomID = szRoomID;
        _szCurRoomPass = szPass;
        
        //trace("_szCurRoomID:"+_szCurRoomID)
        //trace("_szCurRoomPass:"+_szCurRoomPass)
        
        _oThis.getRoomInfo(szRoomID, szPass);
    };
    
    this._checkUserPermissionToJoin = function(aRoomInfo){
        if(aRoomInfo.length > 0){
            //"PERMISSIONGRANTED"
            _oThis.joinRoom(aRoomInfo[0].roomData.id, aRoomInfo[0].roomData.pass);
        }else {
            //"PERMISSIONREFUSED"
            g_oCTLMultiplayer.closeAllDialog();
            g_oCTLMultiplayer.showGeneralDialog(TEXT_WRONG_PASSWORD, "s_oNetworkManager._onPasswordFailed");
        }
    };
    
    this._onPasswordFailed = function(){
        g_oCTLMultiplayer.closeAllDialog();
        g_oCTLMultiplayer.showTypeRoomPassword(_szCurRoomID);
    };
    
    this.getRoomInfo = function(szRoomID, szPass){
        _oCurClient.multiplayer.listRooms(ROOM_TYPE_USEROWNER, {id: szRoomID, pass: szPass}, 0,0, _oThis._onRoomInfoReturned, _oThis.callbackError);
    };
    
    this._onRoomInfoReturned = function(aRoomInfo){
        _oThis._checkUserPermissionToJoin(aRoomInfo);
        
        if(_aCbCompleted[ON_ROOM_INFO_RETURNED]){
            _aCbCompleted[ON_ROOM_INFO_RETURNED].call(_aCbOwner[ON_ROOM_INFO_RETURNED], aRoomInfo);
        }
    };

    this._init();
}

