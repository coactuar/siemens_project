function CMenu(){
    var _oBg;
    var _oButPlay;
    var _oFade;
    var _oAudioToggle;
    var _oCreditsBut;
    
    var _pStartPosCredits;
    var _pStartPosAudio;
    var _pStartPosFullscreen;
    
    var _oButFullscreen;
    var _fRequestFullScreen = null;
    var _fCancelFullScreen = null;    
    
    var _oButLocal;
    var _oButMultiplayer;
    var _iIdTimeout;
    
    this._init = function(){
        _oBg = createBitmap(s_oSpriteLibrary.getSprite('bg_menu'));
        s_oStage.addChild(_oBg);
       
        // var pStartPosButLocal = {x: (CANVAS_WIDTH/2) - 400, y: CANVAS_HEIGHT -250};
        // var oSprite = s_oSpriteLibrary.getSprite('local_but');
        // _oButLocal = new CGfxButton(pStartPosButLocal.x,pStartPosButLocal.y,oSprite, s_oStage);
        // _oButLocal.addEventListener(ON_MOUSE_UP, this._onButLocalRelease, this);
        
        var pStartPosButMulti = {x: (CANVAS_WIDTH/2), y: CANVAS_HEIGHT - 250};
        var oSprite = s_oSpriteLibrary.getSprite('multiplayer_but');
        _oButMultiplayer = new CGfxButton(pStartPosButMulti.x,pStartPosButMulti.y,oSprite, s_oStage);
        _oButMultiplayer.addEventListener(ON_MOUSE_UP, this._onButMultiplayerRelease, this);
     
        // var oSprite = s_oSpriteLibrary.getSprite('but_info');
        // _pStartPosCredits = {x:oSprite.width/2 +10,y:(oSprite.height/2)+10};         
        // _oCreditsBut = new CGfxButton((CANVAS_WIDTH/2),CANVAS_HEIGHT -240,oSprite, s_oStage);
        // _oCreditsBut.addEventListener(ON_MOUSE_UP, this._onCreditsBut, this);
     
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            var oSprite = s_oSpriteLibrary.getSprite('audio_icon');
            _pStartPosAudio = {x: CANVAS_WIDTH - (oSprite.height/2)- 10, y: (oSprite.height/2) + 10};            
            _oAudioToggle = new CToggle(_pStartPosAudio.x,_pStartPosAudio.y,oSprite,s_bAudioActive, s_oStage);
            _oAudioToggle.addEventListener(ON_MOUSE_UP, this._onAudioToggle, this);          
        }

        var doc = window.document;
        var docEl = doc.documentElement;
        _fRequestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
        _fCancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
        
        if(ENABLE_FULLSCREEN === false){
            _fRequestFullScreen = false;
        }
        
        if (_fRequestFullScreen && screenfull.isEnabled){
            oSprite = s_oSpriteLibrary.getSprite("but_fullscreen");
            _pStartPosFullscreen = {x: oSprite.width/2 +10, y: (oSprite.height/2)+10};   
            _oButFullscreen = new CToggle(_pStartPosFullscreen.x,_pStartPosFullscreen.y,oSprite,s_bFullscreen,s_oStage);
            _oButFullscreen.addEventListener(ON_MOUSE_UP,this._onFullscreen,this);
        }

        _oFade = new createjs.Shape();
        _oFade.graphics.beginFill("black").drawRect(0,0,CANVAS_WIDTH,CANVAS_HEIGHT);
        
        s_oStage.addChild(_oFade);
        
        createjs.Tween.get(_oFade).to({alpha:0}, 1000).call(function(){_oFade.visible = false;});  
        
        if(s_oSoundtrack !== null){
            setVolume("soundtrack",1);
        }
        
        this.refreshButtonPos();
    };
    
    this.unload = function(){
        // _oButLocal.unload();
        _oButMultiplayer.unload();
        
        _oFade.visible = false;
        
        // _oCreditsBut.unload();
        
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            _oAudioToggle.unload();
            _oAudioToggle = null;
        }
        
        if (_fRequestFullScreen && screenfull.isEnabled) {
            _oButFullscreen.unload();
        }
        
        s_oStage.removeAllChildren();

        s_oMenu = null;
    };
    
    this.refreshButtonPos = function(){
        // _oCreditsBut.setPosition(_pStartPosCredits.x + s_iOffsetX,s_iOffsetY + _pStartPosCredits.y);
        if(DISABLE_SOUND_MOBILE === false || s_bMobile === false){
            _oAudioToggle.setPosition(_pStartPosAudio.x - s_iOffsetX,s_iOffsetY + _pStartPosAudio.y);
        } 
        
        if (_fRequestFullScreen && screenfull.isEnabled) {
            _oButFullscreen.setPosition(_pStartPosFullscreen.x + s_iOffsetX, _pStartPosFullscreen.y + s_iOffsetY);
        }
    };
    
    this.resetFullscreenBut = function(){
	if (_fRequestFullScreen && screenfull.isEnabled){
		_oButFullscreen.setActive(s_bFullscreen);
	}
    };

    this._onFullscreen = function(){
        if(s_bFullscreen) { 
		_fCancelFullScreen.call(window.document);
	}else{
		_fRequestFullScreen.call(window.document.documentElement);
	}
	
	sizeHandler();
    };
    
    this._onAudioToggle = function(){
        Howler.mute(s_bAudioActive);
        s_bAudioActive = !s_bAudioActive;
    };
    
    this._onCreditsBut = function(){
        new CCreditsPanel();
    };
    
    this._onButLocalRelease = function(){
        s_bMultiplayer = false;
        s_bPlayWithBot = false;
        
        s_oMenu.unload();

        $(s_oMain).trigger("start_session");
        s_oMain.gotoSelectPlayers();
        
    };

    this._onButMultiplayerRelease = function(){
        $(s_oMain).trigger("start_session");

        s_bMultiplayer = true;
        s_bPlayWithBot = false;

        
        s_oNetworkManager.addEventListener(ON_MATCHMAKING_CONNECTION_SUCCESS, this._onMatchmakingConnected);
        s_oNetworkManager.addEventListener(ON_GAMEROOM_CONNECTION_SUCCESS, this.clearBotCheck);
        s_oNetworkManager.addEventListener(ON_BACK_FROM_A_ROOM, this.clearBotCheck);
        s_oNetworkManager.connectToSystem();
    };

    this.onRemoteGameStart = function(iNumPlayers){
        s_oMenu.clearBotCheck();
        
        s_bMultiplayer = true;
        s_bPlayWithBot = false;
        
        s_oMenu.unload();
        s_oMain.gotoGameMulti(iNumPlayers);
    };

    this._onMatchmakingConnected = function(){
        s_oMenu._checkMatchWithBot();
    };

    this._checkMatchWithBot = function(){
        var iTime = randomFloatBetween(18000, 26000);
        _iIdTimeout = setTimeout(function(){
            s_bMultiplayer = true;
            s_bPlayWithBot = true;
            
            
            g_oCTLMultiplayer.closeAllDialog();
            
            s_oNetworkManager.disconnect();
            
            s_oNetworkManager.generateRandomName();
            
            s_oMenu.unload();
            s_oMain.gotoGameWithBot();
        }, iTime);
    };
    
    this.clearBotCheck = function(){
        clearTimeout(_iIdTimeout);
    };
    
    s_oMenu = this;
    
    this._init();
}

var s_oMenu = null;