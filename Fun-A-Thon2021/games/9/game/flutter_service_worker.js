'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "82dfadb7900b766854e37034c84fd68a",
"assets/assets/images/games/games_bg.jpg": "96a479b2987f67d6d3242e39943fe076",
"assets/assets/images/games/game_1.gif": "7a02bc51bfc67de7b913fbe2c4947dd4",
"assets/assets/images/games/game_2.gif": "6566418a8f6daec5c18e5d7036a871de",
"assets/assets/images/games/game_3.gif": "fedee8d47c139915fb79817649a270c1",
"assets/assets/images/games/game_4.gif": "31d65ed3e53104e57a2101394b79033b",
"assets/assets/images/game_one/category_1.1.jpg": "074cd287cedf66358f2f07dc27c21122",
"assets/assets/images/game_one/category_1.2.jpg": "86000e293dcae4dabbf9256c61dae14f",
"assets/assets/images/game_one/category_1.3.jpg": "fe61c24d03ac963af1465e9886c88fc5",
"assets/assets/images/game_one/category_1.4.jpg": "c8aa4d63ccec31168226fc020889ba4a",
"assets/assets/images/game_one/confetti.gif": "e993d191d03335fd09a1987db3f8d39a",
"assets/assets/images/game_one/game_1.1.jpg": "13eee998a2fb92a2764187a4df8fa6f8",
"assets/assets/images/game_one/game_1.2.jpg": "7d5800eafa6b9ce79a16eaa124f28faf",
"assets/assets/images/game_one/game_1.3.jpg": "9436365894816209754087b220f5a88e",
"assets/assets/images/game_one/game_1.4.jpg": "879b5d62b62ee608136ac10502b61b77",
"assets/assets/images/game_one/game_one_bg.jpg": "c76204fb2b63e143259a0184cb085cab",
"assets/assets/images/game_one/solution_1.1.jpg": "b7c707c186b8580b9a447def11e2f974",
"assets/assets/images/game_one/solution_1.2.jpg": "e5ae5faf8b166e616758b07097007ad7",
"assets/assets/images/game_one/solution_1.3.jpg": "83005672fec45e1163d0a0e1c89a9919",
"assets/assets/images/game_one/solution_1.4.jpg": "ea2ad6959db124c306dd91e0f05d6fde",
"assets/assets/images/game_two/category_2.1.jpg": "942ea194761efc491be16d0b5d9784ea",
"assets/assets/images/game_two/category_2.2.jpg": "b0112ee6f4b9cea4ffad868eee78f55c",
"assets/assets/images/game_two/category_2.3.jpg": "961fbffd4f4fc784cea5705356ec7a64",
"assets/assets/images/game_two/category_2.4.jpg": "5fd7b4b3a63ff18814d41f747fc265b9",
"assets/assets/images/game_two/game_2.1.jpg": "83224ef0d7fc99c53d5dbc300da49206",
"assets/assets/images/game_two/game_2.2.jpg": "7df552ff1f89138095476be9b36d65a8",
"assets/assets/images/game_two/game_2.3.jpg": "7faf248d1ea02dd50e7e1057828a0dee",
"assets/assets/images/game_two/game_2.4.jpg": "a79c7e20fdfad5a1f4c6286487b642e9",
"assets/assets/images/game_two/game_two_bg.jpg": "949fb8b2d88150c95762826376decf9e",
"assets/assets/images/game_two/solution_2.1.jpg": "e8a8fe9c494b58bfb33b75c5e881065d",
"assets/assets/images/game_two/solution_2.2.jpg": "b3a4bdb4b08b3211e3be96cb8f066b89",
"assets/assets/images/game_two/solution_2.3.jpg": "bf6023042689103a0ff924df5a3e157f",
"assets/assets/images/game_two/solution_2.4.jpg": "5f489f004ab5ad731f2a392e292bc141",
"assets/assets/images/main/back_button.png": "16d9c6c4f31cc045338dcdcefe6d093b",
"assets/assets/images/main/confetti.gif": "e993d191d03335fd09a1987db3f8d39a",
"assets/assets/images/main/home_page_bg.jpg": "edb1581f884168c5601be70540e6563f",
"assets/assets/images/main/instruction.png": "2f233122217c5d0c41f052026593f15b",
"assets/assets/sound/sound_effect.mp3": "c6860c4debed29e3eda2a2a9fe980dcc",
"assets/assets/sound/zone_1_sound_effect.mp3": "32f1bbcb38b8ccca51ec198ac33cdbe3",
"assets/assets/sound/zone_2_sound_effect.mp3": "606c448299b29d6202118d722a41c89a",
"assets/FontManifest.json": "7b2a36307916a9721811788013e65289",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/NOTICES": "76ea63029d5a7d30c46a9b6a3bfa2604",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"index.html": "1cdfdfff22066edf065dd664d5ab1a98",
"/": "1cdfdfff22066edf065dd664d5ab1a98",
"main.dart.js": "b1d5ddf36a543824cd0ccfa993681cf4",
"manifest.json": "3003e86dbaf434bf591b0c6377fc197a",
"version.json": "3c1774ca32773bcd1c50e80b391fb359"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
