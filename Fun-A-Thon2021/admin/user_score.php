<?php
	//require_once "../controls/sesAdminCheck.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Users</title>
<link rel="stylesheet" type="text/css" href="../assects/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/all.min.css">
<link rel="stylesheet" type="text/css" href="../assects/css/styles.css">

</head>

<body class="admin">
<!-- <nav class="navbar navbar-expand-md bg-light"> -->
  <!--<a class="navbar-brand" href="#"><img src="../img/logo.png" class="logo"></a>-->
  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button> -->

  <!-- <div class="collapse navbar-collapse" id="navbarSupportedContent"> -->
    <!-- <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
         <a class="nav-link" href="users.php">Registered Employe</a> | <a class="nav-link" href="users1.php">Registered Family</a> 
      </li>     
    </ul> -->
    <div class="row login-info links border-bottom border-top p-2">   
    <div class="col-12 text-left text-dark">
        <div class="col-12 text-left text-dark">
        <button class="btn-sm btn-info">
            <a href="users.php">Registered Employe</a>
            </button>
            ||     <button class="btn-sm btn-info"><a href="users1.php">Registered Family</a></button>||
            <button class="btn-sm btn-info"><a href="score.php">Score</a></button>||<button class="btn-sm btn-info"><a href="master.php">Master Leaderboard</a></button>
||<button class="btn-sm btn-info"><a href="feed.php">Feedback</a></button> ||<button class="btn-sm btn-info"><a href="user_score.php">Users Leaderboard</a></button>
||<button class="btn-sm btn-info"><a href="scorefamily.php">Family Leaderboard</a></button>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, </a> <a href="?action=logout">Logout</a>
        </div>
    </div>
    </div>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <!-- <a class="nav-link" href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> -->
      <!-- </li>
      <li class="nav-item">
         <a class="nav-link" href="?action=logout">Logout</a>
      </li>
    </ul>
    
  </div> -->
</nav>

<div class="container-fluid bg-white color-grey">
     
    <div class="row mt-1 p-2">
        <div class="col-12">
            <a href="export_logins.php"><img src="excel.png" height="45" alt=""/> Download </a>
        </div>
    </div>
    <div class="row mt-1 p-2">
        <div class="col-12">
            <div id="user-message"></div>
            <div id="users"> </div>
        </div>
    </div>
</div>


<script src="../assects/js/jquery.min.js"></script>
<script src="../assects/js/bootstrap.min.js"></script>
<script>
$(function(){
    getUsers('1');
});

function update(pageNum)
{
  getUsers(pageNum);
}

function getUsers(pageNum)
{
    $.ajax({
        url: 'score_users.php',
        data: {action: 'getusers', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#users").html(response);
            
        }
    });
    
}

function logoutUser(uid)
{
   $.ajax({
        url: 'ajax.php',
         data: {action: 'logoutuser', userid: uid},
         type: 'post',
         success: function(output) {
             getUsers('1');
         }
   });
}

function updVerify(id)
{
    var verID = '#verify'+id;
    var value = $(verID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'upduserverify', userid :id, ver: value},
        success: function(data){
           // console.log(data);
            if(data == "succ")
            {
                $("#user-message").html('Verification status updated succesfully').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#user-message").html('Verification status could not be updated.').removeClass().addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function updActive(id)
{
    var actID = '#active'+id;
    var value = $(actID).val();
    
    $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: {action: 'upduseractive', userid :id, act: value},
        success: function(data){
           // console.log(data);
            if(data == "succ")
            {
                $("#user-message").html('Active status updated succesfully').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();
            }
            else                    
            {
                $("#user-message").html('Active status could not be updated.').removeClass().addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
            }

            
        }
    });
    
}

function delUser(id)
{
    if(confirm('Are you sure?'))
    {
        $.ajax({
            type: 'POST',
            url: 'ajax_score.php',
            data: {action: 'deluser', userId :id},
            success: function(data){
               // console.log(data);
                if(data == "succ")
                {
                    $("#user-message").html('User deleted.').removeClass().addClass('alert alert-success').fadeIn().delay(2000).fadeOut();
                    getUsers('1');
                }
                else                    
                {
                    $("#user-message").html('User could not be deleted.').removeClass().addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
                }
    
                
            }
        });
    }
    
}
</script>

</body>
</html>