-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2022 at 02:52 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.1.33-42+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `register`
--

-- --------------------------------------------------------

--
-- Table structure for table `MASTER_LEADERBOARD`
--

CREATE TABLE `MASTER_LEADERBOARD` (
  `SESSION_ID` varchar(255) NOT NULL,
  `EMAIL_ID` varchar(255) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `SCORE` int DEFAULT '0',
  `CREATED_AT` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SCORES`
--

CREATE TABLE `SCORES` (
  `SCORE_ID` int NOT NULL,
  `SESSION_ID` varchar(255) NOT NULL,
  `GAME_ID` int NOT NULL,
  `EMAIL_ID` varchar(255) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `SCORE` int NOT NULL DEFAULT '0',
  `GAME_NAME` varchar(255) NOT NULL,
  `CREATED_AT` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `table_feed`
--

CREATE TABLE `table_feed` (
  `id` int NOT NULL,
  `star` varchar(200) NOT NULL,
  `radio` varchar(150) NOT NULL,
  `question` varchar(200) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE `tbl_admins` (
  `id` int NOT NULL,
  `role` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int NOT NULL,
  `userid` varchar(200) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `emailid` varchar(350) NOT NULL,
  `updates` varchar(100) DEFAULT NULL,
  `topic_interest` varchar(255) DEFAULT NULL,
  `country` varchar(150) NOT NULL,
  `checked` varchar(50) NOT NULL,
  `reg_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `token_expire` datetime DEFAULT NULL,
  `verified` int NOT NULL DEFAULT '0',
  `active` int NOT NULL DEFAULT '1',
  `current_room` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `userid`, `first_name`, `emailid`, `updates`, `topic_interest`, `country`, `checked`, `reg_date`, `login_date`, `logout_date`, `token`, `token_expire`, `verified`, `active`, `current_room`) VALUES
(1, '550f1d30', 'Nishu', 'nishu_8989@yahoo.com', NULL, 'nishanth@coact.co.in', 'friend', 'Yes', '2021-09-20 20:56:35', NULL, NULL, '2fe9c4768176', '2021-09-21 20:56:35', 0, 1, NULL),
(2, '983a6f17', 'Adnan Abbas Malik', 'adnanabbasmalik@gmail.com', NULL, 'adnan.abbasmalik@siemens.com', 'Brother', 'Yes', '2021-09-21 16:30:33', NULL, NULL, 'cdbe851c25f4', '2021-09-22 16:30:33', 0, 1, NULL),
(3, 'd78c6a98', 'Subhan Ashraf', 'mo.subhan.ashraf@gmail.com', NULL, 'adil.amjad@siemens.com', 'Son', 'Yes', '2021-09-22 10:20:46', NULL, NULL, '5aed8a0b6eb9', '2021-09-23 10:20:46', 0, 1, NULL),
(4, '04fb5185', 'Qaiser Furqan', 'Princefurqan409@gmail.com', NULL, 'maira.naz.ext@siemens.com', 'Brother', 'Yes', '2021-09-22 20:12:59', NULL, NULL, 'c694cd2b6ba7', '2021-09-23 20:12:59', 0, 1, NULL),
(5, '1ad60a78', 'Musfirah', 'musfirah.asad@gmail.com', NULL, 'asad.zia@siemens.com', 'Wife', 'Yes', '2021-09-22 20:55:26', NULL, NULL, 'de8d1b69816c', '2021-09-23 20:55:26', 0, 1, NULL),
(6, 'f14e48ea', 'neeraj', 'naganeerajreddy9989@gmail.com', NULL, 'neeraj@coact.co.in', 'brother', 'Yes', '2021-09-23 20:47:49', '2021-09-25 15:32:55', '2021-09-25 15:33:55', '9eaba7abd3cb', '2021-09-24 20:47:49', 0, 1, NULL),
(7, 'bfe943ae', 'Hadia', 'hadia.6267@beaconite.edu.pk', NULL, 'asad.zia@siemens.com', 'Daughter', 'Yes', '2021-09-23 21:13:17', NULL, NULL, '0cbb98232175', '2021-09-24 21:13:17', 0, 1, NULL),
(8, 'd9de2822', 'Yaseen Javed', 'yaseenboxer0@gmail.com', NULL, 'maira.naz.ext@siemens.com', 'Brother', 'Yes', '2021-09-23 21:45:11', NULL, NULL, 'a197b03af517', '2021-09-24 21:45:11', 0, 1, NULL),
(10, '4b8e784f', 'rakdhith', 'rakshith@coact.co.in', NULL, 'sanjay@coact.co.in', 'sfddd', 'Yes', '2021-09-24 13:36:43', NULL, NULL, '7b023c6aed50', '2021-09-25 13:36:43', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int NOT NULL,
  `userid` varchar(200) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `emailid` varchar(350) NOT NULL,
  `checked` varchar(50) NOT NULL,
  `reg_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `token` varchar(200) DEFAULT NULL,
  `token_expire` datetime DEFAULT NULL,
  `verified` int NOT NULL DEFAULT '0',
  `active` int NOT NULL DEFAULT '1',
  `current_room` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `userid`, `first_name`, `emailid`, `checked`, `reg_date`, `login_date`, `logout_date`, `token`, `token_expire`, `verified`, `active`, `current_room`) VALUES
(1, '407acb76', 'Nishanth', 'nishanth@coact.co.in', 'Yes', '2021-09-20 20:56:07', '2021-09-25 17:38:18', '2021-09-25 17:39:18', 'ace59595bac0', '2021-09-21 20:56:07', 0, 1, NULL),
(2, '4fc76f54', 'Pooj', 'pooja@coact.co.in', 'Yes', '2021-09-21 01:55:06', NULL, NULL, '386b9e591703', '2021-09-22 01:55:06', 0, 1, NULL),
(3, 'a3fd6ee3', 'Maira Naz', 'maira.naz.ext@siemens.com', 'Yes', '2021-09-21 13:45:39', NULL, NULL, 'eab1f48400d1', '2021-09-22 13:45:39', 0, 1, NULL),
(4, '78e501f5', 'pawan', 'pawan@coact.co.in', 'Yes', '2021-09-21 14:33:52', NULL, NULL, 'ca048e780ab4', '2021-09-22 14:33:52', 0, 1, NULL),
(5, 'f50b8b6b', 'Anushree Natesh', 'Anushree@coact.co.in', 'Yes', '2021-09-21 14:35:45', NULL, NULL, 'ed8380cddc24', '2021-09-22 14:35:45', 0, 1, NULL),
(6, '83b88c1c', 'Rimsha', 'rimsha.touqeer@siemens.com', 'Yes', '2021-09-21 20:20:27', NULL, NULL, 'a4a675cd22cb', '2021-09-22 20:20:27', 0, 1, NULL),
(7, 'a107d9b2', 'Umar Shabbir', 'umar.shabbir@siemens.com', 'Yes', '2021-09-21 20:21:29', NULL, NULL, 'bb23ec45e69e', '2021-09-22 20:21:29', 0, 1, NULL),
(8, '48ac8cbd', 'Mahad Saleem', 'mahad.saleem@siemens.com', 'Yes', '2021-09-21 20:21:55', NULL, NULL, '1f94daeaadc8', '2021-09-22 20:21:55', 0, 1, NULL),
(9, 'e1b490bc', 'Murtaza Hussain', 'm3nzt5@splm.siemens.com', 'Yes', '2021-09-21 20:28:38', NULL, NULL, '5405aaea684c', '2021-09-22 20:28:38', 0, 1, NULL),
(10, 'e1cb68e0', 'Ehtasham Saeed', 'ehtasham.saeed@siemens.com', 'Yes', '2021-09-21 20:39:09', NULL, NULL, 'ca7e9a206198', '2021-09-22 20:39:09', 0, 1, NULL),
(11, '087d63e6', 'Amjad Afzaal', 'm3nr47@splm.siemens.com', 'Yes', '2021-09-21 21:29:37', NULL, NULL, 'e6a047a71265', '2021-09-22 21:29:37', 0, 1, NULL),
(12, 'b77277b3', 'Hafiz Arslan Shabbir', 'HafizArslan.Shabbir@siemens.com', 'Yes', '2021-09-21 21:54:53', NULL, NULL, '81d4e535b32a', '2021-09-22 21:54:53', 0, 1, NULL),
(13, 'cd45c2aa', 'Muhammad Mahad', 'muhmah2j@splm.siemens.com', 'Yes', '2021-09-21 22:11:12', NULL, NULL, 'd372788a2d6b', '2021-09-22 22:11:12', 0, 1, NULL),
(14, 'efa27430', 'asdfad', 'adfadsfad@siemens.com', 'Yes', '2021-09-21 23:01:06', NULL, NULL, '4f1b57776986', '2021-09-22 23:01:06', 0, 1, NULL),
(15, 'c71706bc', 'Vinay Ashi', 'vinay.ashi@siemens.com', 'Yes', '2021-09-22 05:07:52', NULL, NULL, 'ae2b2afcfacf', '2021-09-23 05:07:52', 0, 1, NULL),
(16, '361d08eb', 'Ansar Rasool', 'ansar.rasool@siemens.com', 'Yes', '2021-09-22 09:37:19', NULL, NULL, '35c9622f7586', '2021-09-23 09:37:19', 0, 1, NULL),
(17, 'dd54e7a2', 'Adil Amjad', 'adil.amjad@siemens.com', 'Yes', '2021-09-22 10:19:55', NULL, NULL, '7665b6a22935', '2021-09-23 10:19:55', 0, 1, NULL),
(18, 'bb18cc2d', 'Maria ', 'maria.latif.ext@siemens.com', 'Yes', '2021-09-22 11:05:45', NULL, NULL, 'e35d98fa7da2', '2021-09-23 11:05:45', 0, 1, NULL),
(19, 'a333fae0', 'Awais Belal', 'awais.belal@siemens.com', 'Yes', '2021-09-22 12:14:32', NULL, NULL, 'd3f94e6da2b5', '2021-09-23 12:14:32', 0, 1, NULL),
(20, '0e3e6124', 'Ateeb', 'ateeb.akber@siemens.com', 'Yes', '2021-09-22 13:04:47', NULL, NULL, 'f6d95e39d1ac', '2021-09-23 13:04:47', 0, 1, NULL),
(21, '91e0e152', 'Abu Bakar', 'muhammad.abubakar.ext@siemens.com', 'Yes', '2021-09-22 17:17:19', NULL, NULL, '875f76aa4138', '2021-09-23 17:17:19', 0, 1, NULL),
(22, '1fd4fafb', 'Hafsa Jamal', 'hafsa.jamal@siemens.com', 'Yes', '2021-09-22 18:54:01', NULL, NULL, 'dc8181193f78', '2021-09-23 18:54:01', 0, 1, NULL),
(23, 'db5fea30', 'Asad Zia', 'asad.zia@siemens.com', 'Yes', '2021-09-22 20:39:55', NULL, NULL, '0939b5ba04ea', '2021-09-23 20:39:55', 0, 1, NULL),
(24, 'ffef79d8', 'Muhammad Zeeshan Altaf', 'zeeshan.altaf@siemens.com', 'Yes', '2021-09-23 12:17:47', NULL, NULL, '25e9f1e42766', '2021-09-24 12:17:47', 0, 1, NULL),
(25, '77714cc6', 'Tariq Mehmood', 'm3ns3d@splm.siemens.com', 'Yes', '2021-09-23 12:55:39', NULL, NULL, '9e1480dcf3ee', '2021-09-24 12:55:39', 0, 1, NULL),
(26, 'b3b9e738', 'neeraj', 'neeraj@coact.co.in', 'Yes', '2021-09-23 16:09:10', '2021-09-25 17:34:24', '2021-09-25 17:35:24', 'cd60ffd20220', '2021-09-24 16:09:10', 0, 1, NULL),
(27, 'b919c337', 'Zohaib Ali', 'm3n85z@splm.siemens.com', 'Yes', '2021-09-24 13:09:52', NULL, NULL, 'b0a97d6e67f1', '2021-09-25 13:09:52', 0, 1, NULL),
(28, '55ab45c4', 'sanjay', 'sanjay@coact.co.in', 'Yes', '2021-09-24 13:31:32', NULL, NULL, 'ede5e3442733', '2021-09-25 13:31:32', 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_logins`
--

CREATE TABLE `tbl_user_logins` (
  `id` int NOT NULL,
  `user_id` varchar(200) NOT NULL,
  `join_time` datetime NOT NULL,
  `leave_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `id` int NOT NULL,
  `temp_emailid` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `MASTER_LEADERBOARD`
--
ALTER TABLE `MASTER_LEADERBOARD`
  ADD PRIMARY KEY (`SESSION_ID`);

--
-- Indexes for table `SCORES`
--
ALTER TABLE `SCORES`
  ADD PRIMARY KEY (`SCORE_ID`);

--
-- Indexes for table `table_feed`
--
ALTER TABLE `table_feed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emailid` (`emailid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emailid` (`emailid`);

--
-- Indexes for table `tbl_user_logins`
--
ALTER TABLE `tbl_user_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_user_logins_ibfk_1` (`user_id`);

--
-- Indexes for table `temp`
--
ALTER TABLE `temp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `SCORES`
--
ALTER TABLE `SCORES`
  MODIFY `SCORE_ID` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_feed`
--
ALTER TABLE `table_feed`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_user_logins`
--
ALTER TABLE `tbl_user_logins`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp`
--
ALTER TABLE `temp`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
