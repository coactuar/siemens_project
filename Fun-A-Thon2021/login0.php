
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fun-A-Thon 2021</title>
  <link rel="stylesheet" href="assects/css/bootstrap.min.css">
  <link rel="stylesheet" href="assects/css/all.min.css">
  <link rel="stylesheet" href="assects/css/styles.css">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<style>

@import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');
*{
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: 'Poppins', sans-serif;
}
html,body{
  display: grid;
  height: 100%;
  width: 100%;
  /* place-items: center; */
  background:#101028;
  overflow: auto;
}
::selection{
  background: #fa4299;
  color: #fff;
}
.wrapper{
  overflow: hidden;
  max-width: 350px;
  /* max-height: 500px; */
  /* background: #fff; */
  padding: 5px;
  /* border-radius: 5px; */
  box-shadow: 0px 15px 20px rgba(0,0,0,0.1);
  margin-left: 60px;
}
.wrapper .title-text{
  display: flex;
  width: 200%;
}
.wrapper .title{
  width: 50%;
  font-size: 35px;
  font-weight: 600;
  text-align: center;
  transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55);
}
.wrapper .slide-controls{
  position: relative;
  display: flex;
  height: 50px;
  width: 100%;
  overflow: hidden;
  margin: 10px 0 10px 0;
  justify-content: space-between;
  border: 1px solid lightgrey;
  /* border-radius: 5px; */
}
.slide-controls .slide{
  height: 100%;
  width: 100%;
  color: #fff;
  font-size: 18px;
  font-weight: 500;
  text-align: center;
  line-height: 48px;
  cursor: pointer;
  z-index: 1;
  /* transition: all 0.6s ease; */
}
/* .slide-controls label.signup{
  color: #000;
} */
.slide-controls .slider-tab{
  position: absolute;
  height: 100%;
  width: 50%;
  left: 0;
  z-index: 0;
  /* border-radius: 5px; */
  background:#004659;
  /* transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55); */
}
input[type="radio"]{
  display: none;
}
#signup:checked ~ .slider-tab{
  left: 50%;
}
#signup:checked ~ label.signup{
  color: #fff;
  cursor: default;
  user-select: none;
}
/* #signup:checked ~ label.login{
  color: #000;
}
#login:checked ~ label.signup{
  color: #000;
} */
#login:checked ~ label.login{
  cursor: default;
  user-select: none;
}
.wrapper .form-container{
  width: 100%;
  overflow: hidden;
}
.form-container .form-inner{
  display: flex;
  width: 200%;
}
.form-container .form-inner form{
  width: 50%;
  /* transition: all 0.6s cubic-bezier(0.68,-0.55,0.265,1.55); */
}
.form-inner form .field{
  height: 50px;
  width: 100%;
  margin-top: 4px;
}
.form-select{
    height:100%;
  width: 100%;
  outline: none;
  padding-left: 20px;
  /* border-radius: 5px; */
  border: 1px solid lightgrey;
  border-bottom-width: 2px;
  font-size: 17px;
  /* transition: all 0.3s ease; */
}
.form-inner form .field input {
  height: 100%;
  width: 100%;
  outline: none;
  padding-left: 15px;
  /* border-radius: 5px; */
  border: 1px solid lightgrey;
  border-bottom-width: 2px;
  font-size: 17px;
  /* transition: all 0.3s ease; */
}
.form-inner form .field input:focus{
  border-color: #fc83bb;
  /* box-shadow: inset 0 0 3px #fb6aae; */
}
.form-inner form .field input::placeholder{
  color: #999;
  /* transition: all 0.3s ease; */
}
form .field input:focus::placeholder{
  color: #b3b3b3;
}
.form-inner form .pass-link{
  margin-top: 5px;
}
.form-inner form .signup-link{
  text-align: center;
  margin-top: 30px;
}
.form-inner form .pass-link a,
.form-inner form .signup-link a{
  color: #fa4299;
  text-decoration: none;
}
.form-inner form .pass-link a:hover,
.form-inner form .signup-link a:hover{
  text-decoration: underline;
}
form .btn{
  height: 50px;
  width: 100%;
  /* border-radius: 5px; */
  position: relative;
  overflow: hidden;
}
form .btn .btn-layer{
  height: 100%;
  width: 300%;
  position: absolute;
  left: -100%;
  background: #004659;
  /* border-radius: 5px; */
  /* transition: all 0.4s ease;; */
}
form .btn:hover .btn-layer{
  left: 0;
}
form .btn input[type="submit"]{
  height: 100%;
  width: 100%;
  z-index: 1;
  position: relative;
  background: none;
  border: none;
  color: #fff;
  padding-left: 0;
  /* border-radius: 5px; */
  font-size: 20px;
  font-weight: 500;
  cursor: pointer;
}
@media (max-width: 575.98px) { 
  .wrapper{
  /* overflow: hidden;
  max-width: 350px;
  /* max-height: 500px; */
  /* background: #fff; */
  /* padding: 5px; */
  /* border-radius: 5px; */
  /* box-shadow: 0px 15px 20px rgba(0,0,0,0.1); */
  margin-left: 0px; 
}
}
</style>

<body>
<div class="container-fluid">
          <div class="row">
          <div class="col-12 col-md-5">
<img src="assects/img/Registration.png" class="img-fluid w-50 mt-5 "style="margin-left:50px;" alt="" srcset="">
<img src="assects/img/Fun2.png" class="img-fluid w-75 ml-5 "  style="margin-top:150px; margin-bottom:50px;"alt="" srcset="">
<div class="wrapper ">

  <div class="form-container">
    <div class="slide-controls">
      <input type="radio" name="slide" id="login" checked>
      <input type="radio" name="slide" id="signup">
      <label for="login" class="slide login">Employee</label>
      <label for="signup" class="slide signup">Family</label>
      <div class="slider-tab"></div>
    </div>
<div class="form-inner">
                 
                  <form method="POST" id="login-form" class="login">
                  <h6> <div id="login-message"></div></h6>
                  <!-- <input type="hidden" id="app" name="app" value="<?= $varified; ?>">  -->

        <div class="field">
          <input type="email" placeholder="Employee Email Id" name="email" required>
        </div>
        <div >
 
        </div>
        <div class="field btn" id="btnSubmit1">
          <div class="btn-layer"></div>
          <input type="submit" name="reguser-btn" id="btnSubmit1"  class="form-submit  "  value="Login" />
        </div>
        <a href="https://coactx.live/siemens/Fun-A-Thon2021/index.php" class="offset-md-4">Register Here</a>
        <p style="color: white; font-size:10px">
        In case of any queries contact: Support@coact.co.in</p>
 </form>
 <form action="post" id="login-form1" class="signup">
 <h6> <div id="login-message_fam"></div></h6>
 <!-- <input type="hidden" id="app" name="app1" value="<?= $varified; ?>">  -->

        <div class="field">
          <input type="email" placeholder="Family Email Id" name="email_fam" required>
        </div>
     
       
    
        <div class="field btn" id="btnSubmit" >
          <div class="btn-layer"></div>
          <input type="submit" name="reguser-btn"   class="form-submit  "  value="Login" />
        </div>
        <a href="https://coactx.live/siemens/Fun-A-Thon2021/index.php" class="offset-md-4">Register Here</a>
        <p style="color: white; font-size:10px">
        In case of any queries contact:   Support@coact.co.in</p>
      </form>
                </div>
               
                </div>
  </div>

          </div>
          <div class="col-12 col-md-7">
          <img src="assects/img/Registration12.jpg" class="vh-100" width="100%"  alt="" srcset="">
          </div>
          </div>
      </div>    
                 

<div id="code"></div>

  <script src="assects/js/jquery.min.js"></script>
  <script src="assects/js/bootstrap.min.js"></script>
  <script>

const loginText = document.querySelector(".title-text .login");
const loginForm = document.querySelector("form.login");
const loginBtn = document.querySelector("label.login");
const signupBtn = document.querySelector("label.signup");
const signupLink = document.querySelector("form .signup-link a");
signupBtn.onclick = (()=>{
  loginForm.style.marginLeft = "-50%";
  loginText.style.marginLeft = "-50%";
});
loginBtn.onclick = (()=>{
  loginForm.style.marginLeft = "0%";
  loginText.style.marginLeft = "0%";
});
signupLink.onclick = (()=>{
  signupBtn.click();
  return false;
});</script>

  <script>
    $(document).on('submit', '#login-form', function()
{

    $('#login').attr('disabled', true);
  $.post('login/checklogin_emp.php', $(this).serialize(), function(data)
  {
   
    // alert("hello");
     // console.log(data);
      if(data == 's')
      {
   
          location.href='webcast.php'; 

      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
  </script>
  <script>
    $(document).on('submit', '#login-form1', function()
{


    $('#login').attr('disabled', true);
  $.post('login/checklogin_fam.php', $(this).serialize(), function(data)
  {

     // console.log(data);
      if(data == 's')
      {
        alert(data);
          location.href='webcast.php'; 
      }
      else if (data == '-1')
      {
          $('#login-message_fam').text('You are already logged in. Please logout and try again.');
          $('#login-message_fam').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
      else
      {
          $('#login-message_fam').text(data);
          $('#login-message_fam').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
          $('#login').attr('disabled', false);
          return false;
      }
  });
  
  
  return false;
});
  </script>
 
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-20"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-20');
</script>-->

</body>

</html>