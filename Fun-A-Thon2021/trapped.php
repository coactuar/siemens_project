<?php
// header('Location: http://coact.live/BOOTIntenationalLive2021/');
// exit;
// include('commons/header.php');
?>
<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <link rel="stylesheet" href="assects/css/bootstrap.min.css">
  <link rel="stylesheet" href="assects/css/all.min.css">
  <link rel="stylesheet" href="assects/css/styles.css">

        </head>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
      crossorigin="anonymous" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
<meta name="msapplication-tap-highlight" content="no"/>
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    </head>
    <body>
      
<style>

@import url('https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap');
*{
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: 'Poppins', sans-serif;
}
html,body{
  display: grid;
  height: 100%;
  width: 100%;
  /* place-items: center; */
  background:#101028 !important;
 
}

</style>

<body>
<div class="container-fluid">
          <div class="row">
          <div class="col-12 col-md-6">
<img src="assects/img/Registration.png" class="img-fluid w-25 mt-3  "style="margin-left:50px;" alt="" srcset="">

<img src="assects/img/11.png" class="mt-4 "  width="100%" alt="">

<div class="offset-md-5">
     <a href="games/8/trapped/index.php" target="_blank">     <button class="btn btn-md mt-4  bg-info" value="" > Play Game</button> </a>
 
     </div>

                </div>

          <div class="col-12 col-md-6 ">
<div class="mt-5">

<div class="embed-responsive embed-responsive-16by9  " style="margin-top:80px;">
  <!-- <iframe class="embed-responsive-item" src="videos/mysterious video.mp4"></iframe> -->
  <video src="videos/trapped game video.mp4" controls></video>
</div>
</div>
         
   

          </div>
          </div>
      </div>    
      <div class="leaderboard mt-5">
            <div class="container-fluid ">
              <div class="row ">
                <div class="col-12 col-md-6 col-lg-6 mx-auto">
                  <table class="table table-striped bg-white " id="leaderboard">
                    <thead class="bg-info">
                      <tr class="">
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Score</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>        

          <script>
            const apiUrl = "https://k2k75yq4gh.execute-api.ap-south-1.amazonaws.com/prod";
            $(document).ready(function(){
        //         axios.get(apiUrl + '/leaderboard/26').then(function (res) {
        //   displayLeaderboard(res.data.body);
        //   console.log(res.data.body);
        // })
        // .catch(function (error) {
        //   console.log(error);
        // });
        axios.get(apiUrl + '/leaderboard/30').then(function (res) {
                displayLeaderboard(res.data.body);
            })
                .catch(function (error) {
                    console.log(error);
                });
        const displayLeaderboard = (data) => {
          data = data.sort((a,b) => {
            if(a.score === b.score) {
              return b.updatedAt - a.updatedAt;
            }
            return b.score - a.score;
          })
          let html = '';
          data.forEach((ele, idx) => {
            html += `<tr>
                  <th>${idx + 1}</th>
                  <td>${ele.username}</td>
                  <td>${ele.score}</td>
                </tr>`
          })

          document.querySelector('#leaderboard tbody').innerHTML = html;
        }
    });
        </script>
 
</body>

</html>       