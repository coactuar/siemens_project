<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
<style>

body{
  font-family: 'arial', sans-serif;
  margin:0;

  background:#101028;

}
form{
color:#f5ec00;
font-size:18px;
}
label{
color:white;
}
aside{
max-width:700px;
  margin:auto;
}
.survey-hr{
margin:30px 0;
  border: .5px solid #ddd;
}
.star-rating {
   margin: 25px 0 0px;
  font-size: 0;
  white-space: nowrap;
  display: inline-block;
  width: 175px;
  height: 35px;
  overflow: hidden;
  position: relative;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating i {
  opacity: 0;
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 20%;
  z-index: 1;
  background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
  background-size: contain;
}
.star-rating input {
  -moz-appearance: none;
  -webkit-appearance: none;
  opacity: 0;
  display: inline-block;
  width: 20%;
  height: 100%;
  margin: 0;
  padding: 0;
  z-index: 2;
  position: relative;
}
.star-rating input:hover + i,
.star-rating input:checked + i {
  opacity: 1;
}
.star-rating i ~ i {
  width: 40%;
}
.star-rating i ~ i ~ i {
  width: 60%;
}
.star-rating i ~ i ~ i ~ i {
  width: 80%;
}
.star-rating i ~ i ~ i ~ i ~ i {
  width: 100%;
}


</style>
  <body>
  <div class="container-fluid">

<!-- <div class="">
<img src="assects/img/Registration.png" class=" mt-3 mr-2 float-right" width="12%" alt="" srcset="">
</div>


<div class="">
<img src="assects/img/Logo1.png" class="img-fluid mt-5" width="35%"  alt="" srcset="">

</div> -->

         
<div class=" text-right">

<button class="btn btn-primary btn-sm mt-3 "> <a href="https://coactx.live/siemens/Fun-A-Thon2021/webcast.php" class="text-white">Home</a> </button>
</div>
  <section >


<aside style="padding:15px ;margin-top:15px;">

      
         
 <div id="question" class="mt-5">
              <div id="question-form" class="panel panel-default">
<form method="POST" action="#" class="form panel-body" role="form" >
<div id="ques-message"></div>
<label>1. Your overall experience with us ?</label><br>

<span class="star-rating" required>
<input type="radio" name="rating" value="1" required><i></i>
<input type="radio" name="rating" value="2" required><i></i>
<input type="radio" name="rating" value="3" required><i></i>
<input type="radio" name="rating" value="4" required><i></i>
<input type="radio" name="rating" value="5" required><i></i>
</span>

<hr class="survey-hr"> 
<div class="mt-5">

<label>2. Would you like to have such events in future as well? </label><br>
<span class="" required>
<input type="radio" name="rating1"  value="Yes" required><i class="text-white ml-2">Yes</i>
<br>
<input type="radio" name="rating1"  value="No" required ><i class="text-white ml-2">No</i>
<br>
<input type="radio" name="rating1"  value="Maybe" required><i class="text-white ml-2">Maybe</i>

</span>
</div>
<div class="clear"></div> 
<hr class="survey-hr"> 
<label for="m_3189847521540640526commentText">3. What was the most interesting thing about the event:</label><br/><br/>
<textarea cols="75" name="commentText" rows="1" style="width:520px" required></textarea><br>
<br>
<div class="clear"></div> 
 <input style="background:#43a7d5;color:#fff;padding:5px;border:0" type="submit" value="Submit your review">&nbsp; 

</form> 
              </div>
</div>
</aside>
</section> 
<script>



$(document).on('submit', '#question-form form', function()
  {  
    alert("Thank you for your feedback");
          $.post('feedsubmit.php', $(this).serialize(), function(data)
          {
              if(data=="success")
              {
                $('#ques-message').text('Your Feedback is submitted successfully.');
                $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                $('#question-form').find("textarea").val('');
              }
              else 
              {
                $('#ques-message').text(data);
                $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
              }
              
          });
      
    
    return false;
  });

</script>
    </body>
</html>